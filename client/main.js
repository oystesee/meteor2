/*
Works as a link between the server and client directory,
for example main.js imports the Posts collections from the server directory
*/

import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';
import Posts from './posts.js';
import { Mongo } from 'meteor/mongo';
import './main.jsx';
import { Modal } from 'react-modal';

if (Meteor.isClient) {
    console.log('in the client'); 
    $(".button-collapse").sideNav();
}

Template.posts.helpers({
    posts : function(){
        console.log('templating stuff here');
        return Posts.find({}); 
    }
});
/*
if (Meteor.isClient) {
    Template.projectImageItem.events = {
        "click .open-modal" : function(e,t) {
        e.preventDefault();
        $("#projectImageModal").modal("show");
        }
    };
}
*/