// Reaktor API:  https://github.com/kadirahq/meteor-reaktor
// Router API:   https://github.com/meteorhacks/flow-router

/*
router.jsx routes all the client side components based on the link clikked in the nav-bar written in Header.jsx. 
The content field determines the component that will be rendered in the given route path 
*/

Reaktor.init(
  <Router>
    <Route path="/" content={Home} layout={MainLayout} />
    <Route path="/register" content={Register} layout={MainLayout} triggersEnter = {isLoggedIn} />
    <Route path="/search" content={Search} layout={MainLayout} />
    <Route path="/login" content={SignIn} layout={MainLayout} />
    <Route path="/loggedin" content={Login} layout={MainLayout} />
     <Route path="/collectionTest" content={InsertData} layout={MainLayout} />
  </Router>
);


// checks if the user is loggen in
function isLoggedIn(context, doRedirect){
  console.log(arguments); 
  if(User.isLoggedIn()){
    doRedirect('/loggedin'); 
  }
}

// Reaktor doensn't have a notFound component yet
FlowRouter.notFound = {
  action() {
    ReactLayout.render(MainLayout, { content: <NotFound /> });
  }
};
