/*
This page should display the users personal data from places they have eaten and want to store for later
*/

Login = React.createClass({
    render(){
        return(
            <div>
                <h1> Welcome to your page</h1>
                <p> 
                this is only a possible displayment of how the users data is presented
                </p>
                <table className="highlight">
                    <thead>
                        <tr>
                            <th data-field="id">Name</th>
                            <th data-field="name">Type of Food</th>
                            <th data-field="price">Rating</th>
                        </tr>
                    </thead>

                    <tbody>
                        <tr>
                            <td>Alvin</td>
                            <td>Eclair</td>
                            <td>$0.87</td>
                        </tr>
                        <tr>
                            <td>Alan</td>
                            <td>Jellybean</td>
                            <td>$3.76</td>
                        </tr>
                        <tr>
                            <td>Jonathan</td>
                            <td>Lollipop</td>
                            <td>$7.00</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        )
    }
});