/*
creates new users for the application
*/
Register = React.createClass({
	onSubmit(e) {
		e.preventDefault();
		var ele = $(e.target);

		var email = ele.find("#email").val();
		var password = ele.find("#password").val();
		var confirmPassword = ele.find("#confirmPassword").val();
		if(password === confirmPassword && password !== "" && confirmPassword !== "") {
			var accountInfo = {
				email: email,
				password: password
			};
			Accounts.createUser(accountInfo, function(er) {
				if(er) {
					console.log(er);
					Materialize.toast(er.reason, 4000);
							Materialize.toast('Could not login. Get mad at the developer.', 4000);
						} else {
							Materialize.toast('Your account was created, go the log in page');
						}
				});
		} else {
			Materialize.toast('Your passwords dont match!', 4000);
		}
	},
	render() {
		return (
			<div className="row">
				<h4 className="text-center">Registrer en konto</h4>
				<form onSubmit={this.onSubmit} className="col offset-s4 s4">
					<div className="row">
						<div className="input-field col s12">
							<input id="email" type="text" className="validate" />
							<label htmlFor="email">Email</label>
						</div>
					</div>
					<div className="row">
						<div className="input-field col s12">
							<input id="password" type="password" className="validate" />
							<label htmlFor="password">Password</label>
						</div>
					</div>
					<div className="row">
						<div className="input-field col s12">
							<input id="confirmPassword" type="password" className="validate" />
							<label htmlFor="confirmPassword">Confim password</label>
						</div>
					</div>
					<div className="row">
						<button className="waves-effect waves-light btn btn-block">Submit</button>
					</div>
				</form>
			</div>
		);
	}
});