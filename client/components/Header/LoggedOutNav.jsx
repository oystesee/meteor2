
LoggedOutNav = React.createClass({
	render() {
		return (
        <div>
            <ul className="right hide-on-med-and-down">
              <li><a href="/">Home</a></li>
              <li><a href="/register">Register</a></li>
              <li><a href="/search">Search</a></li>
              <li><a href="/login">Log inn</a></li>

            </ul>
            <ul className="side-nav" id="mobile-demo">
              <li><a href="/">Hjem</a></li>
              <li><a href="/register">Registrer deg</a></li>
              <li><a href="/search">Søk</a></li>
              <li><a href="/login">Log inn</a></li>
            </ul>
        </div>
		);
	}
});