/*
The header component renders to different nav-bars. One for users not logged in and one for logged in users. 
This i determined by the navOptions variable. The different navbars are written in LoggedInNav and LoggedOutNav, 
located in the same directory

The component also routes the user when he logges out or logges in
*/

Header = React.createClass({
  getInitialState() {
      return {
          isLoggedIn: User.isLoggedIn()  
      };
  },
  logout() {
    Meteor.logout((er)=>{
      if(er) {
        Materialize.toast(er.reason, 4000);
      } else {
        this.setState({isLoggedIn: !this.state.isLoggedIn});
        FlowRouter.go('/');
      }
    }).bind(this);
  },
  render() {
    var navStyle = {
      backgroundColor: "#3f51b5", 
      paddingLeft: "12px"
    };
    var navOptions = User.isLoggedIn() ? <LoggedInNav logout={this.logout} /> : <LoggedOutNav />;
    return (
      <nav style={navStyle}>
        <div className="nav-wrapper">
          <a href="/" className="brand-logo">Resturapp</a>
          {navOptions}
        </div>
      </nav>
    );
  }
});