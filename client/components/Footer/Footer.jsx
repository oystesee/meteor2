/*
This component is rather self explanatory, in other words just a footer
*/

Footer = React.createClass({
    render(){
      var footStyle = {
        backgroundColor: "#3f51b5",
      };
        return(
        <footer className="page-footer" style={footStyle}>
          <div className="container">
            <div className="row">
              <div className="col l6 s12">
                <h5 className="white-text"></h5>
                <p className="grey-text text-lighten-4">
                  The source code for this application is avaliable in our bitbucket reposityr
                </p>
              </div>
              <div className="col l4 offset-l2 s12">
                <h5 className="white-text">Utviklere</h5>
                <ul>
                  <li><a className="grey-text text-lighten-3" href="#!">Øystein</a></li>
                  <li><a className="grey-text text-lighten-3" href="#!">Kathrine</a></li>
                  <li><a className="grey-text text-lighten-3" href="#!">Terje</a></li>
                  <li><a className="grey-text text-lighten-3" href="#!">Hung</a></li>
                  <li><a className="grey-text text-lighten-3" href="#!">Henry</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div className="footer-copyright">
            <div className="container">
            © 2016 Group 17 TDT2810
            <a className="grey-text text-lighten-4 right" href="https://bitbucket.org/trondaal/it2810-17-oppgave-3"> Our code</a>
            </div>
          </div>
        </footer>
        )
    }

});
