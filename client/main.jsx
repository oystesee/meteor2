/*
MainLayout controlles the rendering of the pages body, based on the current route 
the main-tag renders in the component between the <Header /> and the <Footer /> 
*/

MainLayout = React.createClass({
  render() {
    return (
      <div id="body">
        <Header />
        <main className="container">{this.props.content}</main>
        <Footer />
      </div>
    );
  }
});
