
MainLayout = React.createClass({
  render() {
    return (
      <div id="body">
        <Header />
        <main className="container">{this.props.content}</main>
        <Footer />
      </div>
    );
  }
});
